import React from 'react';
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom';
import './App.css';

function Home(props) {
  return <h1>ini halaman home</h1>
}

function UserList(props) {
  return (
    <div>
      <h1>ini halaman users</h1>
      <ul>
        <li><Link to='user/ade pranaya'>Ade Pranaya</Link></li>
        <li><Link to='user/robert'>Robert</Link></li>
      </ul>
    </div>
  )
}

function UserDetail({ match }) {
  return <h1>halo {match.params.name}</h1>
}

function NoMatch(props) {
  return <h1>404 Not Found</h1>
}

class App extends React.Component {

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <nav>
            <li><Link to='/'>Home</Link></li>
            <li><Link to='/users'>Users</Link></li>
          </nav>

          <main>
            <Switch>
              <Route path='/' exact component={Home} />
              <Route path='/users' exact component={UserList} />
              <Route path='/user/:name' exact component={UserDetail} />
              <Route component={NoMatch} />
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    );
  }

}

export default App;
